Source: cabocha
Priority: optional
Maintainer: TSUCHIYA Masatoshi <tsuchiya@imc.tut.ac.jp>
Build-Depends: debhelper (>= 12), dh-autoreconf, dh-python, libcrfpp-dev (>= 0.57), libmecab-dev (>= 0.99.3), python3-all-dev, gem2deb, quilt
Standards-Version: 3.9.6
Homepage: https://taku910.github.io/cabocha/
XS-Ruby-Versions: all

Package: cabocha
Section: contrib/misc
Architecture: any
Depends: ${shlibs:Depends}, cabocha-dic (>= ${source:Version}), mecab
Description: A Japenese dependency/case structure analysis system
 CaboCha is a parser which detects dependency/case structures of
 Japanese sentences.

Package: cabocha-utils
Section: contrib/misc
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Support programs of CaboCha
 This package provides the dictionary compiler to convert a dictionary
 written in text format to a binary data for CaboCha, a parser which
 detects dependency/case structures of Japanese sentences.  This
 package is necessary to install dictionary packages for CaboCha like
 cabocha-dic.

Package: cabocha-dic
Section: non-free/misc
Architecture: all
Depends: cabocha-utils, mecab-ipadic-utf8|mecab-jumandic-utf8
Description: Dictionaries of CaboCha encoded in UTF-8
 CaboCha is a parser which detects dependency/case structures of
 Japanese sentences.

Package: cabocha-dic-eucjp
Section: contrib/misc
Architecture: all
Depends: cabocha-utils, cabocha-dic, mecab-ipadic|mecab-jumandic
Description: Dictionaries of CaboCha encoded in EUC-JP
 CaboCha is a parser which detects dependency/case structures of
 Japanese sentences.

Package: libcabocha5
Section: libs
Architecture: any
Depends: ${shlibs:Depends}
Conflicts: libcabocha4
Description: Libraries of Cabocha
 CaboCha is a parser which detects dependency/case structures of
 Japanese sentences.

Package: libcabocha-dev
Section: libdevel
Architecture: any
Depends: libcabocha5 (= ${binary:Version})
Description: Header files of Cabocha
 CaboCha is a parser which detects dependency/case structures of
 Japanese sentences.

Package: libcabocha-perl
Section: perl
Architecture: any
Depends: ${perl:Depends}, ${shlibs:Depends}
Description: Cabocha binding for Perl
 CaboCha is a parser which detects dependency/case structures of
 Japanese sentences.
 .
 libcabocha-perl is its binding for Perl.

Package: python3-cabocha
Section: python
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}
Description: Cabocha binding for Python3
 CaboCha is a parser which detects dependency/case structures of
 Japanese sentences.
 .
 python-cabocha is its binding for Python.

Package: ruby-cabocha
X-DhRuby-Root: ruby
Architecture: any
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Cabocha binding for Ruby language
 CaboCha is a parser which detects dependency/case structures of
 Japanese sentences.
 .
 ruby-cabocha is its binding for Ruby language.

